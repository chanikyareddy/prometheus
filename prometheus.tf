

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "9090"
        to_port     = "9090"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "prometheus" {
    ami                         =  var.prometheus
    instance_type               = "t2.large"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.prometheus.public_ip
    }

     provisioner "remote-exec" {
    inline = [
"sudo useradd --no-create-home --shell /bin/false prometheus",
"sudo useradd --no-create-home --shell /bin/false node_exporter",
"sudo mkdir /etc/prometheus",
"sudo mkdir /var/lib/prometheus",
"sudo chown prometheus:prometheus /etc/prometheus",
"sudo chown prometheus:prometheus /var/lib/prometheus",
"cd ~",
"curl -LO https://github.com/prometheus/prometheus/releases/download/v2.15.1/prometheus-2.15.1.linux-amd64.tar.gz",
"sha256sum prometheus-2.15.1.linux-amd64.tar.gz",
"tar xvf prometheus-2.15.1.linux-amd64.tar.gz",
"sudo cp prometheus-2.15.1.linux-amd64/prometheus /usr/local/bin/",
"sudo cp prometheus-2.15.1.linux-amd64/promtool /usr/local/bin/",
"sudo chown prometheus:prometheus /usr/local/bin/prometheus",
"sudo chown prometheus:prometheus /usr/local/bin/promtool",
"sudo cp -r prometheus-2.15.1.linux-amd64/consoles /etc/prometheus",
"sudo cp -r prometheus-2.15.1.linux-amd64/console_libraries /etc/prometheus",
"sudo chown -R prometheus:prometheus /etc/prometheus/consoles",
"sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries",
"rm -rf prometheus-2.15.1.linux-amd64.tar.gz prometheus-2.15.1.linux-amd64",
"sudo tee -a /etc/prometheus/prometheus.yml > /dev/null <<EOT
global:
  scrape_interval: 15s
scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['http://3.84.28.144:9090/']
EOT",
"sudo chown prometheus:prometheus /etc/prometheus/prometheus.yml",
"sudo -u prometheus /usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries",
"sudo tee -a /etc/systemd/system/prometheus.service > /dev/null <<EOT
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

 

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

 

[Install]
WantedBy=multi-user.target
EOT",
"sudo systemctl daemon-reload",
"sudo systemctl start prometheus",

 

"sudo apt-get update",
"sudo apt-get install apache2-utils",
"sudo apt install nginx",
"chmod 777 /etc/nginx/.htpasswd",
"htpasswd -b -c /etc/nginx/.htpasswd chanu chani",
"sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/prometheus",
"cd /etc/nginx/sites-available",
"sudo sed -i '51s/.*/auth_basic "Prometheus server authentication";/' prometheus",
"sudo sed -i "53i proxy_pass http://3.84.28.144:9090/" prometheus",
"sudo sed -i "55i proxy_set_header Upgrade $http_upgrade;" prometheus",
"sudo sed -i "56i proxy_set_header Connection 'upgrade';" prometheus",
"sudo sed -i "57i proxy_set_header Host $host;" prometheus",
"sudo sed -i "58i proxy_cache_bypass $http_upgrade;" prometheus",
"sudo rm /etc/nginx/sites-enabled/default",
"sudo ln -s /etc/nginx/sites-available/prometheus /etc/nginx/sites-enabled/",
"sudo nginx -t",
"sudo systemctl reload nginx",
"sudo systemctl status nginx",
 
    ]
  }
   tags = {
    Name = "prometheus"
  }
}